/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.petshop.DAO;

import br.com.petshop.model.Animal;

/**
 *
 * @author Rick Novo
 */
public class AnimalDAO extends DAO<Animal> {

    public AnimalDAO() {
        super(Animal.class);
    }
    
     public void salvar(Animal animal) {
        em.getTransaction().begin();
        em.persist(animal);
        em.getTransaction().commit();
        em.close();
    }

}
