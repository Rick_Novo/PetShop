/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.petshop.DAO;

import br.com.petshop.model.Cliente;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Rick Novo
 */
public class ClienteDAO extends DAO<Cliente>{

    public ClienteDAO() {
        super(Cliente.class);
    }
    
public List<Cliente> findClientes(String nome) {
        List<Cliente> listaCliente = new ArrayList<>();
        try {
            this.beginTransaction();

            String jpql = "from Cliente c where c.nome like :arg1 ";

            Query query = em.createQuery(jpql);

            if (nome != null && !nome.equals("")) {
                query.setParameter("arg1", nome + "%");
            }

            listaCliente = query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listaCliente;
    }
    
    
}
