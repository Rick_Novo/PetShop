/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.petshop.DAO;

import br.com.petshop.model.Endereco;

/**
 *
 * @author Rick Novo
 */
public class EnderecoDAO extends DAO<Endereco>{

    public EnderecoDAO() {
        super(Endereco.class);
    }
    
}
