/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.petshop.bean;

import br.com.petshop.DAO.AnimalDAO;
import br.com.petshop.model.Animal;
import br.com.petshop.model.Cliente;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


/**
 *
 * @author Rick Novo
 */@ManagedBean
@SessionScoped
 public class AnimalBean extends Bean{
    
    private Animal animal = new Animal();
    private final AnimalDAO dao = new AnimalDAO();
 
    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }
    
     public Cliente getCliente() {
        return this.animal.getCliente();
    }

    public void setCliente(Cliente cliente) {
        this.animal.setCliente(cliente);
    }
    
      public String salvar(){
        try{
        dao.salvar(animal);
        addMessageInfo("Animal cadastrado.");
        this.animal = new Animal();
        
        }catch (Exception ex){
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
        return null;
    }

}
