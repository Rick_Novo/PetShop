/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.petshop.bean;

import br.com.petshop.DAO.AnimalSexoDAO;
import br.com.petshop.model.AnimalSexo;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Rick Novo
 */@ManagedBean
 @RequestScoped
public class AnimalSexoBean {

     private AnimalSexo animalSexo = new AnimalSexo();
     private final AnimalSexoDAO dao = new AnimalSexoDAO();

      public List<AnimalSexo> getAnimaisSexo() {
        List<AnimalSexo> lista;
        dao.beginTransaction();
        lista = dao.findAll();
        dao.closeTransaction();
        
        return lista;
    }

    public AnimalSexo getAnimalSexo() {
        return animalSexo;
    }

    public void setAnimalSexo(AnimalSexo animalSexo) {
        this.animalSexo = animalSexo;
    }
     
}
