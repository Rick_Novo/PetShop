/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.petshop.bean;

import br.com.petshop.DAO.CidadeDAO;
import br.com.petshop.model.Cidade;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Rick Novo
 */@ManagedBean
 @ViewScoped
public class CidadeBean extends Bean{
    
    private Cidade cidade =  new Cidade();
    private final CidadeDAO dao = new CidadeDAO();

    
      public CidadeBean() {
    }
    
    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }
    
}
