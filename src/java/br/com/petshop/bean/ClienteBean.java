/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.petshop.bean;

import br.com.petshop.DAO.ClienteDAO;
import br.com.petshop.model.Cliente;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Rick Novo
 */
@ManagedBean
@ViewScoped
public class ClienteBean extends Bean {

    private Cliente cliente = new Cliente();
    private ClienteDAO dao = new ClienteDAO();

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String salvar() {
        try {
            dao.beginTransaction();
            dao.save(cliente);
            dao.commitAndCloseTransaction();
            addMessageInfo("Cliente cadastrado.");
            this.cliente = new Cliente();

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
        return null;
    }

    public String atualizar() {
        try {
            dao.beginTransaction();
            dao.update(cliente);
            dao.commitAndCloseTransaction();
            addMessageInfo("Cliente Atualizado.");
            this.cliente = new Cliente();

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }

        return null;
    }

    public List<Cliente> listacliente(String complete) {
        List<Cliente> lista;

        lista = dao.findClientes(complete);

        return lista;
    }

}
