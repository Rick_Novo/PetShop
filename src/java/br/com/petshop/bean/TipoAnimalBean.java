/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.petshop.bean;

import br.com.petshop.DAO.TipoAnimalDAO;
import br.com.petshop.model.TipoAnimal;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Rick Novo
 */
@ManagedBean
@ViewScoped
public class TipoAnimalBean {

    private TipoAnimal tipoAnimal = new TipoAnimal();
    private TipoAnimalDAO dao = new TipoAnimalDAO();

    public List<TipoAnimal> getTipoAnimais() {
        List<TipoAnimal> lista;
        dao.beginTransaction();
        lista = dao.findAll();
        dao.closeTransaction();

        return lista;
    }

    public TipoAnimal getTipoAnimal() {
        return tipoAnimal;
    }

    public void setTipoAnimal(TipoAnimal tipoAnimal) {
        this.tipoAnimal = tipoAnimal;
    }

}
