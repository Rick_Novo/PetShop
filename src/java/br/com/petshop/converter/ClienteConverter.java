/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.petshop.converter;


import br.com.petshop.DAO.ClienteDAO;
import br.com.petshop.model.Cliente;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.application.FacesMessage;
import javax.faces.convert.ConverterException;

/**
 *
 * @author sala303b
 */
@FacesConverter("clienteConverter")
public class ClienteConverter implements Converter {

    private ClienteDAO  dao  = new ClienteDAO();
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && value.trim().length() > 0) {
            try {
                Cliente cliente = dao.find(Integer.parseInt(value));
                return cliente;
            } catch (Exception e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro de conversão", "Not a valid theme."));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object != null) {
            return String.valueOf(((Cliente) object).getId());
        } else {
            return null;
        }
    }
}
