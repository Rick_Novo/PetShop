/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.petshop.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author Rick Novo
 */@Entity
public class Animal extends Entidade implements Serializable{
    
    private String nome;
    private String raca;
    private String sexo;
    private int idade;

    @ManyToOne
    private Cliente cliente;

    @ManyToOne
    private TipoAnimal tipoAnimal;

    @ManyToOne
    private AnimalSexo animalSexo ;
    
    public Animal(){
        this.cliente = new Cliente();
        this.animalSexo = new AnimalSexo();
        this.tipoAnimal = new TipoAnimal();
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public TipoAnimal getTipoAnimal() {
        return tipoAnimal;
    }

    public void setTipoAnimal(TipoAnimal tipoAnimal) {
        this.tipoAnimal = tipoAnimal;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public AnimalSexo getAnimalSexo() {
        return animalSexo;
    }

    public void setAnimalSexo(AnimalSexo animalSexo) {
        this.animalSexo = animalSexo;
    }

}
