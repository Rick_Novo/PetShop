/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.petshop.model;

import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author Rick Novo
 */@Entity
public class Bairro extends Entidade implements Serializable{
    
    private String NomeBairro;

    public String getNomeBairro() {
        return NomeBairro;
    }

    public void setNomeBairro(String NomeBairro) {
        this.NomeBairro = NomeBairro;
    }
    
}
